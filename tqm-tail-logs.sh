#!/bin/bash

# clear

# ----------------------------------------------------------------------
#
# wrote this to run a tail all logs for TQ mobile app
#
# ----------------------------------------------------------------------

#CONSTANTS
userpath=/Users/markeccleston
tqmlogs=$userpath/Library/Application\ Support/CxAlloyTQ/Logs
lastlog=$(ls -rt "$tqmlogs" | tail -1)
path=$tqmlogs/$lastlog

# echo
# echo "WE'RE WATCHING MAMP LOGS"
# echo "============================"
# echo

tail -f "$path"