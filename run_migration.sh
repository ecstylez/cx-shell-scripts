#!/bin/bash

clear

# ----------------------------------------------------------------------
#
# script to easily run a migration
#
# ----------------------------------------------------------------------

echo
echo "WE'RE RUNNING A MIGRATION."
echo "========================================"
echo

# select platform
read -p "Which platform? (TQ or FM) : " input

# choose a project
if [[ $input = "tq" ]]; then
	project=cxalloy-tq;
else
	project=cxalloy-fm-svn2git;
fi

#CONSTANTS
parentDir="~/Work"
path="$parentDir/$project"
exitMsg="abort mission. goodbye"

# select migration number
read -p "Is there a migration number that you'd like to use ? " migrationNumberInput

# set migration number

if [[ $migrationNumberInput = "no" || $migrationNumberInput = "No" || $migrationNumberInput = "n" || $migrationNumberInput = "" ]]; then
	migration="";
	migrationString="a new migration"
else
	migration=$migrationNumberInput;
	migrationString="migration (#$migration)"
fi

echo
echo "----------------------------------------"
echo
echo "you will be running $migrationString here : "
echo $path
echo
read -p "Is this okay? " yesOrNo

# initiate script
if [[ $yesOrNo = "y" || $yesOrNo = "yes" ]]; then

	echo
	echo "Starting migration script..."

	eval cd $path
	eval "php index.php cli/migrate/run/$migration"

	echo
	echo "========================================"
	echo "ALL DONE!"
	echo
	echo "Migration script completed."
	echo
	echo
else
	echo $exitMsg
fi
