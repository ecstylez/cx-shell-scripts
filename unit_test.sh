#!/bin/bash

echo
echo "========================================"
echo

# ----------------------------------------------------------------------
#
# script to easily run a migration
#
# ----------------------------------------------------------------------

#CONSTANTS
parentDir="~/Work"
project=cxalloy-tq
path="$parentDir/$project/"

# go to tests directory
eval cd "$path/application/tests"

# run php unit
eval "$path/vendor/bin/phpunit"

echo
echo "========================================"
echo
