#!/bin/bash

clear

# ----------------------------------------------------------------------
#
# the only thing you'll need to update is the `parentDir` constant
# to match your repo path, everything else "should" work as advertised
#
# ----------------------------------------------------------------------

#CONSTANTS
nowTime=$(date +"%Y%m%d%H%I%S")
parentDir="~/Work"
path="$parentDir/cxalloy-fm-svn2git/application/migrations"
exitMsg="abort mission. goodbye"


echo
echo "WE'RE CREATING A NEW MIGRATION FILE NAME"
echo "========================================"
echo
read -p "What's the file name ? " input

if [[ $input = "" ]]; then
	echo "No filename entered. $exitMsg"
else

	filename=${nowTime}_${input}.php

	echo "the file to be created will be '$filename' and it will be created here:"
	echo
	echo $path
	echo
	read -p "Is this okay?" yesOrNo

	if [[ $yesOrNo = "y" || $yesOrNo = "yes" ]]; then

		eval cd $path
		touch $filename

		echo "========================================"
		echo "ALL DONE!"
		echo
		echo "'$filename' has been created in '$path'"
		echo
		echo
	else
		echo $exitMsg
	fi
fi
