#!/bin/bash

# clear

# ----------------------------------------------------------------------
#
# wrote this to keep up with the formatting of our commit structure
# i.e. git commit -m "[#123456] this is my commit message"
#
# ----------------------------------------------------------------------

#CONSTANTS
exitMsg="Someone didn't follow the instructions... aborting."

# FUNCTIONS
commitFiles () {
	local storyId=$storyId
	local commitMsg=$commitMsg
	local commitString="[$storyId] $commitMsg"

	if [[ $storyId != "" ]]; then
		git commit -m "$commitString"
		# echo $commitString
	fi
}

echo
echo "WE'RE COMMITTING OUR CHANGES"
echo "============================"
echo

read -p "What's the Story ID? " storyId

if [[ $storyId = "" ]]; then
	echo "No story ID entered. $exitMsg"
else
	echo "the story id is '$storyId'"
	echo
	read -p "What's your commit message? " commitMsg

	commitFiles
fi