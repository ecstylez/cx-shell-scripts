#!/bin/bash

# ----------------------------------------------------------------------
#
# wrote this to clean up local branches when the remote branch(es)
# no longer exists
#
# ----------------------------------------------------------------------

#CONSTANTS
excludebranches="master|develop"
currentbranch=$(git rev-parse --abbrev-ref head)
exitMsg="abort mission. goodbye"
search=$(git branch --merged develop | egrep -v "(^\*|$currentbranch|$excludebranches)")

echo
echo "LOCAL BRANCHES TO BE DELETED"
echo "============================"
echo "$search"
echo "============================"
echo
read -p "We are cleaning out the local branches above. Is this okay? " response

if [[ $response = "y" || $response = "yes" ]]; then

	echo "$search" | xargs git branch --delete

	echo
	echo "========================================"
	echo "ALL DONE!"
	echo
else
	echo $exitMsg
fi