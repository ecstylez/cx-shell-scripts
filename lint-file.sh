#!/bin/bash

# clear

# ----------------------------------------------------------------------
#
# this is a shell script to help you along with the automated part of
# the linting. remembering cli command can be a headache at times
#
# ----------------------------------------------------------------------

#CONSTANTS
eslint=$(npm bin)/eslint
standardsfile=coding_standards/eslint.json
exitMsg="abort mission. goodbye"

echo
echo "WE'RE GOING TO EXPEDITE 'SOME' OF THE LINTING"
echo "============================================="
echo
read -p "Which file do you want to line ? " -e inputfile

if [[ $inputfile = "" ]]; then
	echo "No filename entered. $exitMsg"
else
	$eslint --config $standardsfile $inputfile  --fix
fi