#!/bin/bash

# clear

# ----------------------------------------------------------------------
#
# wrote this to run a tail on all MAMP logs as i keep forgetting
# the path and command
#
# ----------------------------------------------------------------------

echo
echo "WE'RE TAILING LOGS."
echo "========================================"
echo
read -p "Which platform? (TQ or FM) : " input

# choose a project
if [[ $input = "tq" ]]; then
	project=cxalloy-tq;
else
	project=cxalloy-fm-svn2git;
fi

#CONSTANTS
userpath="/Users/markeccleston"
cilogs="$userpath/Work/$project/application/logs"
lastlog=$(ls -rt $cilogs | tail -1)
path="/Applications/MAMP/logs/* $cilogs/$lastlog"

# echo
# echo "WE'RE WATCHING MAMP LOGS"
# echo "============================"
# echo

tail -f $path