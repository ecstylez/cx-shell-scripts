#!/bin/bash

# ----------------------------------------------------------------------
#
# if you're not running gulp, do npm intall -g browser-sync
# after that, this script will work
#
# ----------------------------------------------------------------------

# CONSTANTS
localhost="localhost:8888/cxalloy-fm-svn2git/"

browser-sync start --proxy $localhost --files "application/**/*.php, css/*.css, js/**/*.js"